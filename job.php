<?php

include_once 'Planeta.php';
include_once 'Recta.php';
include_once 'Figuras.php';

// Creamos la conexion
$conn = mysqli_connect("localhost", "root", "", "examen_meli");

// Verificamos la conexion
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Definimos los planetas con sus distancias al sol y velocidades angulares
$ferengui = new Planeta(500, 1);
$betasoide = new Planeta(2000, 3);
$vulcano = new Planeta(1000, -5);

// Definimos el perimetro maximo previamente calculado por otra corrida de este job
$perimetroMaximo = 148.12543265629;

// Calculamos la cantidad de dias de aca a 10 años para tener en cuenta los años bisiestos, etc
$fechaFin = strtotime('+10 years');
$today = time();
$difference = $fechaFin - $today;
$diasTotales = floor($difference / 86400);

// Inicializamos los periodos con sequia, lluvia, etc
$cantPeriodosSequia = $cantPeriodosLluvia = $cantPicoIntensidad = $cantCondOptimas = 0;

// El job va a correr desde el dia 1 (inicial) hasta el ultimo dia
for ($dia = 1; $dia <= $diasTotales; $dia++) {
    // Calculo la posicion actual de cada planeta para un día dado
    $posicionFerengui = $ferengui->getCoordenadas($dia);
    $posicionBetasoide = $betasoide->getCoordenadas($dia);
    $posicionVulcano = $vulcano->getCoordenadas($dia);
    $posicionSol = [0, 0];

    // Armamos la recta en base a dos planetas
    $rectaFerenguiBetasoide = new Recta($posicionFerengui, $posicionBetasoide);
    $rectaBetasoideVulcano = new Recta($posicionBetasoide, $posicionVulcano);

    // Inicializamos la clase figuras pasando las posiciones de todo el sistema solar
    $figuras = new Figuras($posicionFerengui, $posicionBetasoide, $posicionVulcano, $posicionSol);

    // Los tres planetas estan alineados
    if ($figuras->rectasAlineadas($rectaBetasoideVulcano, $rectaFerenguiBetasoide)) {

        // Hay período de sequía porque los planetas alineados al SOL
        if ($figuras->alineadosAlSol($rectaFerenguiBetasoide)) {
            $cantPeriodosSequia += 1;

            $conn->query("INSERT INTO dias (dia, sequia) VALUES ($dia, 1)");
        } // Planetas alineados entre sí pero no con el SOL
        else {
            $cantCondOptimas += 1;

            $conn->query("INSERT INTO dias (dia, cond_opt_pyt) VALUES ($dia, 1)");
        }
    } else {
        /// Los planetas no estan alineados, vamos a ver si el sol pertenece al triangulo formado ///

        if ($figuras->solPerteneceAlTriangulo()) {
            if ($figuras->trianguloPerimetroMaximo($perimetroMaximo)) {

                $conn->query("INSERT INTO dias (dia, pico_lluvia) VALUES ($dia, 1)");

                $cantPicoIntensidad += 1;
            } else {
                $conn->query("INSERT INTO dias (dia, lluvia) VALUES ($dia, 1)");

                $cantPeriodosLluvia += 1;
            }
        }
    }
}

/*
echo "<br>Cant Periodos de sequia: " . $cantPeriodosSequia;
echo "<br>Cant Periodos de lluvia: " . $cantPeriodosLluvia;
echo "<br>Cant Periodos de picos de intensidad: " . $cantPicoIntensidad;
echo "<br>Cant Periodos de condiciones optimas: " . $cantCondOptimas;
*/