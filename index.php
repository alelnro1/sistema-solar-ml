<?php

// Creamos la conexion
$conn = new mysqli(null, "root", "examenMeli", $database, null, "/cloudsql/examen-ingreso-meli:southamerica-east1:examen-meli");

// Verificamos la conexion
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Obtenemos el dia que nos solicitan
$dia_solicitado = $_GET['dia'];

// Buscamos en la tabla dias
$resultado = $conn->query("SELECT * FROM dias WHERE dia = '$dia_solicitado'");

if ($resultado->num_rows > 0) {

    while ($row = $resultado->fetch_assoc()) {
        if ($row["sequia"] == 1) {
            $clima = "sequia";
        } else if ($row["lluvia"] == 1) {
            $clima = "lluvia";
        } else if ($row["pico_lluvia"] == 1) {
            $clima = "pico lluvia";
        } else if ($row["cond_opt_pyt"] == 1) {
            $clima = "condiciones optimas de presion y temperatura";
        }
    }
} else {
    $clima = "normal";
}

echo json_encode([
    "dia" => $dia_solicitado,
    "clima" => $clima
]);